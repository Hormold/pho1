<?php
use Rain\Tpl;
$app->get('/sazha/:pid',function($pid) use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$longip = ip2long($_SERVER['REMOTE_ADDR']);
	$stmt = $db->prepare("INSERT IGNORE INTO sazha1(photo_id,longip) VALUES(?,?)");
	$stmt->bind_param('dd', $pid , $longip);
	$stmt->execute();
	$stmt->close();
	die("ok");
});
$app->get('/tosmall/:pid',function($pid) use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$longip = ip2long($_SERVER['REMOTE_ADDR']);
	$stmt = $db->prepare("INSERT IGNORE INTO sazha1(photo_id,longip,type) VALUES(?,?,2)");
	$stmt->bind_param('dd', $pid , $longip);
	$stmt->execute();
	$stmt->close();
	die("Спасибо, мы скоро удалим эту страничку.");
});
$app->get('/top',function() use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$stmt = $db->query("SELECT * FROM votes WHERE 1 LIMIT 5000");
	$a=Array();$list=Array();
	while ($row=$stmt->fetch_row()) {
		if(!isset($a[$row[1]])){$a[$row[1]]=1;$list[]=$row[1];}else{$a[$row[1]]++;}
	}
	arsort($a);
	$tpl = new Tpl;
	$tpl->assign("list",implode(",",$list));
	$tpl->assign("top",$a);
	$tpl->assign("tmp","top");
	$tpl->assign("dir",$data["dir"]);
	$tpl->draw( "layout" );
	die;
});

$app->get('/top/:city',function($city) use($data){
	include('cfg.php');
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$id=intval($city);
	$stmt = $db->query("SELECT * FROM `hits` WHERE `city_id`='{$id}' ORDER by hits DESC LIMIT 100");
	$a=Array();
	$list=Array();
	while ($row=$stmt->fetch_row()) {
		$a[$row[0]]=$row[1];
		$list[]=$row[0];
	}
	$tpl = new Tpl;
	$tpl->assign("list",implode(",",$list));
	$tpl->assign("top",$a);
	$tpl->assign("tmp","top2");
	$tpl->assign("dir",$data["dir"]);
	$tpl->draw( "layout" );
	die;
});

$app->get('/vote/:pid',function($pid) use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$longip = ip2long($_SERVER['REMOTE_ADDR']);
	
	$stmt = $db->prepare("SELECT * FROM votes WHERE pid = ? AND ip = ?");
	$stmt->bind_param('dd', $pid,$longip);
	$stmt->execute();
	$stmt->bind_result($user_id,$ip);
	while ($stmt->fetch()) {
		die("Вы уже голосовали!");
	}
	
	
	$stmt = $db->prepare("INSERT IGNORE INTO votes(pid,ip) VALUES(?,?)");
	$stmt->bind_param('dd', $pid , $longip);
	$stmt->execute();
	$stmt->close();
	die("Ваш голос учтен!");
});
$app->get('/profile/:id',function($id) use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$tpl = new Tpl;
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	# берем фотки из базы
	$stmt = $db->prepare("SELECT `pid`, `user_id`, `group_id`, `src`, `src_preview`, `src_thumb`, `wall_id` FROM main WHERE user_id = ?");
	$stmt->bind_param('d', $id);
	$stmt->execute();
	$stmt->bind_result($photo_id, $user_id, $group_id, $src, $src_preview, $src_thumb, $wall_id);
	$list=Array();
	$photos = array();
	$i=0;
	while ($stmt->fetch()) {	
		$photo = array();
		$photo['id']=$i;
		$photo['photo_id'] = $photo_id;
		$photo['user_id'] = $user_id;
		$photo['group_id'] = $group_id;
		$photo['src'] = $src;
		$photo['src_preview'] = $src_preview;
		$photo['src_thumb'] = $src_thumb;
		
		if (is_null($wall_id))
			$link_suffix = 'photo-'.$group_id.'_'.$photo_id;
		else
			$link_suffix = 'wall-'.$group_id.'_'.$wall_id;
		$photo['link_suffix'] = $link_suffix;
		$list[]=$group_id;
		array_push($photos, $photo);
		$i++;
	}

	$stmt->close();
	
	# берем общую инфу из базы
	$stmt = $db->prepare("SELECT display_name, photo_200_orig, c.city_title, c.city_id FROM users u LEFT OUTER JOIN cities c ON c.city_id = u.city_id WHERE user_id = ?");
	$stmt->bind_param('d', $id);
	$stmt->execute();
	$stmt->bind_result($display_name, $photo_200_orig, $city_title, $city_id);
	$tpl->assign("photo_200_orig",$photo_200_orig);
	$stmt->fetch();
	$stmt->close();
	if (count($photos) == 0){
		$tpl->assign("error","Пользователя с таким ид нету в базе");
	}else {
		$code="<a href='https://vk.com/id$user_id'><h2>$display_name</h2></a>\n";
		$code.="<a href='get_by_city.php?id=$city_id'><p>$city_title</p></a>\n";
		$code.="<div class='col-md-9'>";
		foreach($photos as &$photo) {
			$code.='<div id="pid'.$photo['photo_id'].'">';
			$code.='<a  class="list-group-item col-md-2" style="width:160px;height:160px;" rel="group" href="'.$photo['src_preview'].'"><img style="" src="'.$photo['src_thumb'].'" /></a>';
			$code.="</div>\n";
		}
		$code.="</div>";
		$tpl->assign("code",$code);
		$tpl->assign("photos",json_encode($photos));
	}
	$tpl->assign("tmp","profile");
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("id",$id);
	$tpl->draw( "layout" );
});
