<?php
use Rain\Tpl;

$app->map('/get/group', function () use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$dir=$data["dir"];
	
	if(isset($_GET["id"])){
		if(intval($_GET["id"])==$_GET["id"] AND intval($_GET["id"])!==0){
			$id=intval($_GET["id"]);
		}else{
			$s1=resl($_GET['id']);
			
			$id = resl2($s1);
		}
		
		if(trim($id)=="" or $id==false){
			die("Bad id. Example: https://vk.com/obtyashka");
		}
	}else{
		die("no id");
	}
	
	if (isset($_GET['offset']))
	{
		$offset = $_GET['offset'];
		if (ctype_digit($offset) == false)
			die('invalid offset');
	}
	else
		$offset = 0;
	$max = 16;
	$offset=intval($offset);

	$tpl = new Tpl;
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","group");
	$stmt = $db->query("SELECT DISTINCT user_id FROM main WHERE group_id = {$id} LIMIT $offset,$max");
	$cnt = 0;
	$offset1 = $offset+1;
	$list=Array();
	$code="<ol start='$offset1'>";
	while ($row=$stmt->fetch_row()) {
		$user_id=$row[0];
		$code.="<div class='list-group-item col-md-3'>";
		$code.="<a target='_blank' href='".$dir."/profile/$user_id' id='u{$user_id}'></a>";
		$code.="</div>\n";
		$list[]=$user_id;
		$cnt++;
	}
	$code.="</ol>";
	
	if ($cnt == 0)
		$code="<p>(пусто) :(</p>";
	else {
		$offset_prev = $offset - $max;
		$offset_next = $offset + $max;
		
		
		if ($offset != 0)
			$buttons="<a class='btn btn-success' href='".$dir."get/group?id=$id&offset=$offset_prev'>&lt;&lt;&lt;&lt;&lt; туда</a>";
		else
			$buttons="&nbsp;";
		
		$buttons.="<a class='btn btn-success' href='".$dir."get/group?id=$id&offset=$offset_next'>&gt;&gt;&gt;&gt; сюда </a>";
	}
	$stmt->close();
	$tpl->assign("id",$id);
	$tpl->assign("list",implode(",",$list));
	$tpl->assign("buttons",$buttons);
	$tpl->assign("code",$code);
	$tpl->draw( "layout" );
})->via("POST","GET");


$app->map('/get/group2', function () use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$dir=$data["dir"];
	if(intval($_GET["id"])==$_GET["id"] AND intval($_GET["id"])!==0){
		$id=intval($_GET["id"]);
		
	}else{
		$id=resl($_GET['id']);
		$id = resl2($id);
	}
	
	if(trim($id)=="" or $id==false){
		die("Bad id. Example: https://vk.com/obtyashka");
	}
	$ids_str_q=getUsers($id);
	
	if (isset($_GET['offset']))
	{
		$offset = $_GET['offset'];
		if (ctype_digit($offset) == false)
			die('invalid offset');
	}
	else
		$offset = 0;
	$max = 16;
	$offset=intval($offset);
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8"); # на всякий пожарный


	$tpl = new Tpl;
	
	$stmt = $db->prepare("SELECT DISTINCT m.user_id, CASE WHEN u.display_name IS NULL THEN m.user_id ELSE u.display_name END FROM main m
LEFT JOIN users u ON (m.user_id = u.user_id) 
WHERE m.user_id IN ($ids_str_q) ORDER BY m.user_id");
	$stmt->execute();
	$stmt->bind_result($user_id, $display_name);
	$list=Array();
	$cnt = 0;
	$code="<ol>";
	$list=Array();
	while ($stmt->fetch()) {
		$code.=sprintf ("<li class='list-group-item col-md-3'><a href='{$dir}profile/%d' id='user-%d'>%s</a></li>". "\n", $user_id,$user_id, $display_name);
		$cnt++;
		$list[]=$user_id;
	}
	$code.="</ol>";
	
	if ($cnt == 0)
		$code="<p>К сожалению, база маленькая. Никого не нашли.</p>";
		
	$stmt->close();

	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","group2");
	$tpl->assign("id",$id);
	$tpl->assign("list",implode(",",$list));
	
	$tpl->assign("code",$code);
	$tpl->draw( "layout" );
})->via("POST","GET");
