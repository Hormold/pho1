<?php
use Rain\Tpl;

$app->get('/cities/search', function () use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	include('parsers/parser_tools.php');
	if(!isset($_GET['name'])){die("no id");}
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);$db->set_charset("utf8");
	$city_title = $_GET['name'];
	
	$stmt = $db->prepare("SELECT city_id FROM cities WHERE city_title = ?");
	$stmt->bind_param('s', $city_title);
	$stmt->execute();
	$stmt->bind_result($city_id);
	
	while ($stmt->fetch())
	{	
		echo "$city_id";
		header("Location: ".$data["dir"]."cities/$city_id");
		$stmt->close();
		die();
	}
	header("Location: ".$data["dir"]."cities");die;
});

$app->get('/cities',function() use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$tpl = new Tpl;
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	$code="";
	
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","city");
	$result = $db->query("SELECT t.city_id, city_title, t.cnt FROM cities g RIGHT OUTER JOIN (SELECT city_id, COUNT(*) as cnt FROM users WHERE sex = 1 GROUP BY city_id HAVING city_id IS NOT NULL) t ON t.city_id = g.city_id ORDER BY t.cnt DESC LIMIT 50");
	//echo $db->error;
	
	while ($row = $result->fetch_row()) {
		$code.='<tr>';
		$code.=sprintf('<td><a href="'.$data["dir"].'cities/%d">%s</a></td>', $row[0], $row[1]);
		$code.=sprintf('<td>%d</td>', $row[2]);
		$code.="</tr>";
	}
	$tpl->assign("code",$code);
	$tpl->draw( "layout" );
	
});

$app->get('/cities/:id',function($id) use($data,$mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname){
	$tpl = new Tpl;
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$db->set_charset("utf8");
	if (isset($_GET['offset']))
	{
		$offset = $_GET['offset'];
		if (ctype_digit($offset) == false)
			die('invalid offset');
	}
	else
		$offset = 0;
	$max = 36;
	
	$dir=$data["dir"];
	
	$stmt = $db->prepare("SELECT city_id, city_title FROM cities WHERE city_id = ?");
	$stmt->bind_param('d', $id);
	$stmt->execute();
	$stmt->bind_result($city_id, $city_title);
	$stmt->fetch();
	$stmt->close();
	

	$code="";
	
	$stmt = $db->prepare("SELECT user_id, display_name, photo_200 FROM users WHERE city_id = ? AND sex = 1 LIMIT ?,?");
	$stmt->bind_param('ddd', $id, $offset, $max);
	$stmt->execute();
	$stmt->bind_result($user_id, $display_name, $ava);
	$tpl->assign("city",$city_title);
	$tpl->assign("city_id",$city_id);
	$cnt = 0;
	$offset1 = $offset+1;
	$code="<ol start='$offset1'>";
	while ($stmt->fetch()) {
		$code.="<div class='list-group-item col-md-3'>";
		$code.="<a target='_blank' href='{$dir}profile/$user_id'><img src='$ava' /><br><h4>$display_name</h4></a>";
		$code.="</div>\n";
		$cnt++;
	}
	$code.="</ol>";
	$buttons="";
	if ($cnt == 0)
		$code="<p>(пусто) :(</p>";
	else {
		$offset_prev = $offset - $max;
		$offset_next = $offset + $max;
		
		
		if ($offset != 0)
			$buttons.="<a class='btn btn-success btn-lg' href='{$dir}cities/$id?offset=$offset_prev'>Назад</a>";
		else
			$buttons.="&nbsp;";
		
		$buttons.="<a class='btn btn-success btn-lg' href='{$dir}cities/$id?offset=$offset_next'>Дальше </a>";
	}
		
	$stmt->close();
	
	$stmt = $db->query("SELECT count(*) FROM users WHERE city_id = ".intval($id)." AND sex = 1");
	$row=$stmt->fetch_row();
	$stmt->close();
	
	$tpl->assign("count",$row[0]);
	$tpl->assign("buttons",$buttons);
	$tpl->assign("code",$code);
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("post","1");
	$tpl->assign("tmp","city");
	$tpl->draw( "layout" );
	
});